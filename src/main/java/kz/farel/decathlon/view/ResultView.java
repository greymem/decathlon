package kz.farel.decathlon.view;

import kz.farel.decathlon.model.Athlete;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
public class ResultView implements Serializable {
    @XmlElement(name = "athlete")
    private List<Athlete> athletes;

    public ResultView(List<Athlete> athletes) {
        this.athletes = athletes;
    }

    public ResultView() {
        // no-arg default constructor for serialization
    }

    @XmlTransient
    public List<Athlete> getAthletes() {
        return athletes;
    }
}
