package kz.farel.decathlon.util;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileUtils {

    private static final String SEMICOLON = ";";


    public static List<List<String>> readCSVFile(File file) throws IOException {
        String fileName = file.getName();
        if (!fileName.toLowerCase().endsWith(".csv")) {
            throw new FileNotFoundException("Wrong file extension");
        }

        List<List<String>> rows = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(file);
             Reader isr = new InputStreamReader(fis);
             BufferedReader br = new BufferedReader(isr)) {

            String cp;
            String separator = null;
            while ((cp = br.readLine()) != null) {
                if (cp.length() == 0) continue;
                if (separator == null) {
                    separator = cp.contains(SEMICOLON) ? SEMICOLON : ",";
                }

                List<String> cols = Arrays.asList(cp.split(separator));
                rows.add(cols);
            }
        }

        return rows;
    }

    public static File saveAsXmlFile(Serializable object, File folder, String resultFileName) throws IOException {
        try {
            File target = new File(folder, resultFileName);
            if (target.exists()) {
                Files.delete(target.toPath());
            }
            final JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
            final Marshaller marshaller = jaxbContext.createMarshaller();
            try (StringWriter writer = new StringWriter();
                 FileOutputStream fos = new FileOutputStream(target)) {
                marshaller.marshal(object, writer);
                fos.write(writer.toString()
                        .getBytes(StandardCharsets.UTF_8)
                );
            }

            return target;
        } catch (JAXBException jbe) {
            throw new RuntimeException("Couldn't serialize object to XML", jbe);
        }
    }

    public static String getFolderPath(File file) {
        String path = file.getAbsolutePath();
        int lastIndexOfSeparator = path.lastIndexOf(File.separatorChar);
        return lastIndexOfSeparator > 0 ? path.substring(0, lastIndexOfSeparator) : "";
    }

    private FileUtils() {
        // Hided private constructor of utility class
    }
}
