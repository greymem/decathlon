package kz.farel.decathlon.constants;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum Event {
    M100(25.4347, 18, 1.81, PointUnit.SECONDS),
    LONG_JUMP(0.14354, 220, 1.4, PointUnit.CENTIMETRES),
    SHOT_PUT(51.39, 1.5, 1.05, PointUnit.METRES),
    HIGH_JUMP(0.8465, 75, 1.42, PointUnit.CENTIMETRES),
    M400(1.53775, 82, 1.81, PointUnit.SECONDS),
    M110_HURDLES(5.74352, 28.5, 1.92, PointUnit.SECONDS),
    DISCUS_THROW(12.91, 4, 1.1, PointUnit.METRES),
    POLE_VAULT(0.2797, 100, 1.35, PointUnit.CENTIMETRES),
    JAVELIN_THROW(10.14, 7, 1.08, PointUnit.METRES),
    M1500(0.03768, 480, 1.85, PointUnit.MINUTES);

    double a;
    double b;
    double c;
    PointUnit unit;

    Event(double a, double b, double c, PointUnit unit) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.unit = unit;
    }

    public BigDecimal calc(String performance) {
        double calcResult;
        switch (unit) {
            case MINUTES:
                int indexOfFirstPoint = performance.indexOf('.');
                String minutesPart = performance.substring(0, indexOfFirstPoint);
                String otherPart = performance.substring(indexOfFirstPoint + 1, performance.length() - 1);

                int minutes = Integer.parseInt(minutesPart);
                double seconds = Double.parseDouble(otherPart);
                seconds += minutes * 60;
                calcResult = calcPointsForTime(seconds);
                break;
            case SECONDS:
                calcResult = calcPointsForTime(Double.parseDouble(performance));
                break;
            case CENTIMETRES:
                double meters = Double.parseDouble(performance);
                calcResult = calcPointsForDistance(meters * 100);
                break;
            default:
                calcResult = calcPointsForDistance(Double.parseDouble(performance));
        }

        // rounding
        return BigDecimal.valueOf(calcResult)
                .setScale(3, RoundingMode.HALF_UP);
    }

    private double calcPointsForTime(double seconds) {
        return a * (Math.pow(b - seconds, c));
    }

    private double calcPointsForDistance(double metres) {
        return a * (Math.pow(metres - b, c));
    }
}
