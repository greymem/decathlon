package kz.farel.decathlon.constants;

public enum PointUnit {
    MINUTES,
    SECONDS,
    METRES,
    CENTIMETRES,
}
