package kz.farel.decathlon;

import kz.farel.decathlon.constants.Event;
import kz.farel.decathlon.model.Athlete;
import kz.farel.decathlon.model.AthleteResult;
import kz.farel.decathlon.util.FileUtils;
import kz.farel.decathlon.view.ResultView;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class DecathlonMain {
    public static final String SILENT_ARG = "--silent";
    public static final String DO_NOT_SAVE_FILE_ARG = "--noResultFile";
    public static final String RESULT_SUFFIX = "_result.xml";

    private boolean isSilent = false;
    private boolean isWithFile = true;
    private String[] args;

    public DecathlonMain(String[] args) {
        this.args = args;
    }

    public static void main(String[] args) {
        if (args == null || args.length > 3 || args.length == 0) {
            System.err.println("Usage: java -jar decathlon.jar <sourceFileName>.csv [" + SILENT_ARG + "] [" + DO_NOT_SAVE_FILE_ARG + "]");
            return;
        }

        DecathlonMain app = new DecathlonMain(args);
        app.calc();
    }

    public List<Athlete> calc() {
        String filePath = args[0];
        readArgs(args);

        try {
            File sourceFile = new File(filePath);
            List<Athlete> result = calc(sourceFile);
            saveAsXmlFile(result,
                    FileUtils.getFolderPath(sourceFile),
                    sourceFile.getName() + RESULT_SUFFIX);
            return result;
        } catch (IOException ioe) {
            throw new RuntimeException("Couldn't read/write file ", ioe);
        }
    }

    private void readArgs(String[] args) {
        List<String> argsList = Arrays.asList(args);
        argsList.remove(0); // remove filename from args

        isSilent = argsList.contains(SILENT_ARG);
        isWithFile = !argsList.contains(DO_NOT_SAVE_FILE_ARG);
    }

    private List<Athlete> calc(File sourceFile) throws IOException {
        List<List<String>> rows = FileUtils.readCSVFile(sourceFile);
        List<Athlete> result = new ArrayList<>();

        for (List<String> row : rows) {
            String name = row.get(0);
            Athlete athlete = new Athlete(name);

            Event[] events = Event.values();
            for (int colIndex = 1; colIndex < row.size(); colIndex++) {
                Event event = events[colIndex - 1];
                String performance = row.get(colIndex);

                athlete.addResult(new AthleteResult(event, performance));
            }

            result.add(athlete);
        }
        printResult(result);

        List<Athlete> sortedResult = sortResult(result);
        if (!isSilent) {
            System.out.println();
            System.out.println("Sorted: ");
        }
        printResult(sortedResult);

        return sortedResult;
    }

    private void printResult(List<Athlete> result) {
        if (isSilent) return;
        result.forEach(a -> System.out.println(a.toString()));
    }

    private List<Athlete> sortResult(List<Athlete> result) {
        List<Athlete> sorted = result.stream()
                .sorted(Comparator.comparing(Athlete::getTotalPoints).reversed())
                .collect(java.util.stream.Collectors.toList());
        addPlacesInfo(sorted);
        return sorted;
    }

    private void addPlacesInfo(List<Athlete> sorted) {
        int currentPlace = 1;

        for (int upperA = 0; upperA < sorted.size(); upperA++) {
            Athlete upperAthlete = sorted.get(upperA);
            if (upperAthlete.getPlace() != null) continue;

            List<Athlete> samePointsAthletes = new ArrayList<>();
            BigDecimal upperAthleteTotalPoints = upperAthlete.getTotalPoints();
            for (int lowerA = upperA + 1; lowerA < sorted.size(); lowerA++) {
                Athlete lowerAthlete = sorted.get(lowerA);

                // lowerAthlete.totalPoints == upperAthlete.totalPoints
                if (lowerAthlete.getTotalPoints().compareTo(upperAthleteTotalPoints) == 0)
                    samePointsAthletes.add(lowerAthlete);
                else break;
            }

            String currentPlaceStr = String.valueOf(currentPlace);
            if (!samePointsAthletes.isEmpty()) {
                int toPlace = currentPlace + samePointsAthletes.size();
                String places = currentPlaceStr + "-" + toPlace;
                upperAthlete.setPlace(places);
                samePointsAthletes.forEach(ua -> ua.setPlace(places));

                currentPlace = toPlace + 1;
            } else {
                upperAthlete.setPlace(currentPlaceStr);
                currentPlace++;
            }
        }
    }

    private void saveAsXmlFile(List<Athlete> source, String folderPath, String resultFileName) throws IOException {
        if (!isWithFile) return;

        ResultView xmlView = new ResultView(source);
        File resultFile = FileUtils.saveAsXmlFile(xmlView, new File(folderPath), resultFileName);
        if (!isSilent) {
            System.out.println();
            System.out.println("Results stored in file: " + resultFile.getAbsolutePath());
        }
    }

}
