package kz.farel.decathlon.model;

import kz.farel.decathlon.constants.Event;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.math.BigDecimal;

public class AthleteResult implements Serializable {
    @XmlElement
    Event event;

    @XmlElement
    String performance;

    @XmlElement
    BigDecimal points;

    public AthleteResult(Event event, String performance) {
        this.event = event;
        this.performance = performance;
        this.points = event.calc(performance);
    }

    public AthleteResult() {
        // no-arg default constructor for deserialization
    }

    @XmlTransient
    public BigDecimal getPoints() {
        return points;
    }

    @Override
    public String toString() {
        StringBuilder line = new StringBuilder();
        line.append(' ')
                .append(event.name())
                .append(" (")
                .append("performance: ").append(performance)
                .append(", points: ").append(points)
                .append(") |");

        return line.toString();
    }

}
