package kz.farel.decathlon.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class Athlete implements Serializable {
    private static final String STR_COLUMN_SEPARATOR = " |";

    @XmlElement
    private String name;

    @XmlElement
    private String place;

    @XmlElement(name = "result")
    private List<AthleteResult> results = new LinkedList<>();

    public Athlete(String name) {
        this.name = name;
    }

    public Athlete() {
        // no-arg default constructor for deserialization
    }

    public void addResult(AthleteResult athleteResult) {
        results.add(athleteResult);
    }

    @XmlElement
    public BigDecimal getTotalPoints() {
        return results.stream()
                .map(AthleteResult::getPoints)
                .reduce(new BigDecimal(0), BigDecimal::add);
    }

    @XmlTransient
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @XmlTransient
    public String getName() {
        return name;
    }

    @XmlTransient
    public List<AthleteResult> getResults() {
        return results;
    }

    @Override
    public String toString() {
        StringBuilder line = new StringBuilder();
        if (place != null) {
            line.append("Place(s) №:").append(place).append(STR_COLUMN_SEPARATOR).append(' ');
        }
        line.append("Athlete name: ").append(name).append(STR_COLUMN_SEPARATOR);

        results.forEach(r -> line.append(r.toString()));

        line.append(" Total points: ").append(getTotalPoints());

        return line.toString();
    }
}
