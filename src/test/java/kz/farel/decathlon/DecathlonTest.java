package kz.farel.decathlon;

import kz.farel.decathlon.model.Athlete;
import kz.farel.decathlon.model.AthleteResult;
import kz.farel.decathlon.util.FileUtils;
import kz.farel.decathlon.view.ResultView;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.*;

public class DecathlonTest {

    @Test
    public void forManualConsoleTest() {
        URL url = getResourceUrl("results_main.csv");
        String[] args = {url.getPath()};
        DecathlonMain.main(args);
        assertTrue(true);
    }

    @Test
    public void checkWinnerName() {
        URL url = getResourceUrl("results_1.csv");
        List<Athlete> result = calc(url);
        assertEquals("Mr. Miyagi", result.get(0).getName());
    }

    @Test
    public void places23() {
        URL url = getResourceUrl("results_23.csv");
        List<Athlete> result = calc(url);
        String expectedPlaces = "2-3";
        assertEquals(expectedPlaces, result.get(1).getPlace());
        assertEquals(expectedPlaces, result.get(2).getPlace());
    }

    @Test
    public void places12_57() {
        URL url = getResourceUrl("results_12-57.csv");
        List<Athlete> result = calc(url);
        String expectedPlaces = "1-2";

        assertEquals(expectedPlaces, result.get(0).getPlace());
        assertEquals(expectedPlaces, result.get(1).getPlace());

        expectedPlaces = "5-7";
        assertEquals(expectedPlaces, result.get(4).getPlace());
        assertEquals(expectedPlaces, result.get(5).getPlace());
        assertEquals(expectedPlaces, result.get(6).getPlace());
    }

    @Test
    public void points700() {
        checkIAAF2001("points700.csv", 700);
    }

    @Test
    public void points800() {
        checkIAAF2001("points800.csv", 800);
    }

    @Test
    public void points900() {
        checkIAAF2001("points900.csv", 900);
    }

    @Test
    public void points1000() {
        checkIAAF2001("points1000.csv", 1000);
    }

    private void checkIAAF2001(String fileName, int expected) {
        URL url = getResourceUrl(fileName);
        List<Athlete> calcResult = calc(url);
        Athlete solo = calcResult.get(0);
        List<AthleteResult> results = solo.getResults();
        for (AthleteResult result : results) {
            float points = result.getPoints().floatValue();
            int iaaf2001 = Math.round(points / 100) * 100;
            assertEquals(expected, iaaf2001);
        }
    }

    private List<Athlete> calc(URL url) {
        String[] args = {url.getPath(), DecathlonMain.SILENT_ARG, DecathlonMain.DO_NOT_SAVE_FILE_ARG};
        DecathlonMain main = new DecathlonMain(args);
        return main.calc();
    }

    @Test
    public void xmlUnmarshalling() throws JAXBException {
        URL url = getResourceUrl("results_main.csv");
        String[] args = {url.getPath(), DecathlonMain.SILENT_ARG};
        List<Athlete> result = new DecathlonMain(args).calc();
        File sourceFile = new File(url.getPath());
        String folderPath = FileUtils.getFolderPath(sourceFile);

        File targetFile = new File(folderPath, sourceFile.getName() + DecathlonMain.RESULT_SUFFIX);

        final JAXBContext jaxbContext = JAXBContext.newInstance(ResultView.class);
        final Unmarshaller um = jaxbContext.createUnmarshaller();
        ResultView view = (ResultView) um.unmarshal(targetFile);

        assertEquals(result.toString(), view.getAthletes().toString());
    }

    private URL getResourceUrl(String resourceName) {
        URL url = this.getClass().getClassLoader().getResource(resourceName);
        assertNotNull(url);
        return url;
    }
}
